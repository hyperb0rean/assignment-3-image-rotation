
#ifndef IMAGE_FUNCTIONS_H
#define IMAGE_FUNCTIONS_H


#include <stdint.h>
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image create_image (uint64_t width, uint64_t height);

struct image rotate( struct image* img );

struct image negative( struct image* img);


void destroy_image (struct image* img);

#endif //IMAGE_FUNCTIONS_H
